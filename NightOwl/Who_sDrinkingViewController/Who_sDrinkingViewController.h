//
//  Who_sDrinkingViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/22/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDelegate.h"
#import "DataFetchProcess.h"
#import "CustomLoadingView.h"

@class NightOwlAppDelegate;

@interface Who_sDrinkingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomDelegate>
{
     NightOwlAppDelegate    *appDelegate;
    
    NSMutableArray     *drinkingCategoryArr;
    
    int   selectCategoryDrink;
}

@property (weak, nonatomic) IBOutlet UIImageView *drinkingBackImage;

- (IBAction)didClickOnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *drinkingTableView;
@property(nonatomic,readwrite)int  indexPage;

-(void)resetOrientation_ForWhosDrinking;

-(void)showLoadingViewForDrinking;

@property (strong, nonatomic) IBOutlet CustomLoadingView *loadingForDrinking;




@end
