//
//  NightOwlViewController.m
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "NightOwlViewController.h"
#import "CustomCatagoryView.h"
#import "NightOwlAppDelegate.h"
#import "Who_sPlayingViewController.h"
#import "Who_sDrinkingViewController.h"
#import "Who_sPartyingViewController.h"
#import "Who_sEatingViewController.h"
#import "Who_sWatchingViewController.h"

@interface NightOwlViewController ()

@end


@implementation UINavigationController (rotation)
//temp hack for iOS6, this allows passing supportedInterfaceOrientations to child viewcontrollers.
- (NSUInteger)supportedInterfaceOrientations {
   return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation NightOwlViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBarHidden=YES;
    
    currentIndex=0;
    checkScroll=0;
   
    justLaunched=YES;
    appDelegate=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    if(justLaunched)
    {
        [self showSplashImageView];
    }
    
   [self  loadHomePage];
    

}




-(void)viewWillAppear:(BOOL)animated
{
    [super  viewWillAppear:animated];
    
    if(appDelegate.isLandScape==1){
        
        appDelegate.isLandScape=0;
        
        [self  resetOrientation_MediaViewer];
    }
    
}

#pragma mark-
#pragma mark  Load Home view

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y != 0) {
        CGPoint offset = sender.contentOffset;
        offset.y = 0;
        sender.contentOffset = offset;
    }
}
-(void)loadHomePage
{
    
    CGRect RectimageScroll=CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height);//ORIGIN_Y//-(ORIGIN_Y+k_ContantHeight)
    _catagoryScrollerView.frame= RectimageScroll;
    _catagoryScrollerView.contentSize = CGSizeMake(_catagoryScrollerView.frame.size.width *5, _catagoryScrollerView.frame.size.height);
    
    NSLog(@"contentsizeheight=%f",_catagoryScrollerView.contentSize.height);
     NSLog(@"contentsizewidth=%f",_catagoryScrollerView.contentSize.width);
    
    //_catagoryScrollerView
    
    _catagoryScrollerView.contentOffset = CGPointMake(_catagoryScrollerView.frame.size.width * currentIndex, 0);

   // [self LoadScrollviewWithCategory:currentIndex-1 CategoryTag:TAG_PREVIOUS_PAGE];
  //  [self  resetOrientation_MediaViewer];
    [self LoadScrollviewWithCategory:currentIndex CategoryTag:TAG_CURRENT_PAGE];
    [self  resetOrientation_MediaViewer];
    [self LoadScrollviewWithCategory:currentIndex+1 CategoryTag:TAG_NEXT_PAGE];
    [self  resetOrientation_MediaViewer];
}


#pragma mark-
#pragma mark  Load Splash Screen

- (void)showSplashImageView
{
    CGRect rectFrame = self.view.frame;
    rectFrame.origin.y = 0;
    splashImageView = [[UIImageView alloc] initWithFrame:rectFrame];
    splashImageBackgroundView = [[UIView alloc] initWithFrame:rectFrame];
    [splashImageBackgroundView setBackgroundColor:[UIColor blackColor]];
    [splashImageView setBackgroundColor:[UIColor clearColor]];
    
     CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,568)];
            
        }
        else{
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,480)];
            
        }
        
    }else if(deviceOrientation == UIInterfaceOrientationLandscapeRight || deviceOrientation == UIInterfaceOrientationLandscapeLeft){
        
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash_Land.png"];
            [splashImageView setFrame:CGRectMake(0,0,568,320)];
            splashImageView.center = CGPointMake(320/2,568/2);
            splashImageView.transform = CGAffineTransformMakeRotation((M_PI/180)*90);
        }
        else{
            splashImageView.image = [UIImage imageNamed:@"splash_Land.png"];
            [splashImageView setFrame:CGRectMake(0,0,480,320)];
            splashImageView.center = CGPointMake(320/2,568/2);
            splashImageView.transform = CGAffineTransformMakeRotation((M_PI/180)*90);
        }
        
    }else{
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,568)];
            
        }
        else{
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,480)];
            
        }
    }

    splashImageView.alpha = 1.0f;
    splashImageBackgroundView.alpha = 1.0f;
    [self.view addSubview:splashImageBackgroundView];
    [self.view addSubview:splashImageView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:2.0f];
    [UIView setAnimationDelegate:(id)self];
    [UIView setAnimationDidStopSelector:@selector(removeSplashImageView)];
    [splashImageView setAlpha:0.0f];
    [UIView commitAnimations];
}

- (void)removeSplashImageView
{
    [splashImageView removeFromSuperview];
    splashImageView = nil;
    [splashImageBackgroundView removeFromSuperview];
    splashImageBackgroundView = nil;
    justLaunched = NO;
    
}

#pragma mark-

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setCatagoryScrollerView:nil];
    [super viewDidUnload];
}

#pragma mark-
#pragma mark  Orientation

-(BOOL)shouldAutorotate
{
    
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
   UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
     [self  resetOrientation_MediaViewer];
    return   UIInterfaceOrientationMaskAll;
    
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

    appDelegate.orientation=toInterfaceOrientation;
     [self  resetOrientation_MediaViewer];
    
    if(justLaunched){
        
        [self  rotateSplashScreen];
    }
}


#pragma mark-
#pragma mark  rotate splash screen

-(void)rotateSplashScreen
{
    [ splashImageView  setImage:nil];
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,568)];
            [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 320, 568)];
        }
        else{
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,480)];
             [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 320, 480)];
        }
        
    }else if(deviceOrientation == UIInterfaceOrientationLandscapeRight || deviceOrientation == UIInterfaceOrientationLandscapeLeft){
        
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash_Land.png"];
            [splashImageView setFrame:CGRectMake(0,0,568,320)];
            [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 568, 320)];
           
        }
        else
        {
            splashImageView.image = [UIImage imageNamed:@"splash_Land.png"];
            [splashImageView setFrame:CGRectMake(0,0,480,320)];
            [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 480, 320)];
        }
        
    }else{
        
        if (screenSize.height==568.0) {
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,568)];
             [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 320, 568)];
        }
        else{
            splashImageView.image = [UIImage imageNamed:@"splash.png"];
            [splashImageView setFrame:CGRectMake(0,0,320,480)];
             [splashImageBackgroundView  setFrame:CGRectMake(0, 0, 320, 480)];
            
        }
    }

}


#pragma mark-
#pragma mark  ScrollView delegates

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    @try
    {
        //Get Page Number
        
                   
            CGFloat pageWidth = scrollView.frame.size.width;
            int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            
            
            //Remove Previous view from scroll view.
            if(scrollView.tag == kCATEGORY_SCROLLER_TAG)
            {
                
               if(page!= currentIndex)
               {
                    
                    currentIndex=page;
                    
                    
                    [self RemovePreviousViewsAndLoadViewsWithIndexNo:page];
                    
                }
            }
       
        
    }
    @catch (NSException *exception) {
        ;
    }
    
   
    
}
-(void)LoadScrollviewWithCategory:(int)pageNumber CategoryTag:(int)_categoryTag
{
    
    if(pageNumber < 0)
        return;
    
    [self displayNewCategory:pageNumber CategoryTag:_categoryTag];
}



#pragma mark-
#pragma mark  Display New slide View

-(void)displayNewCategory:(int)index CategoryTag:(int)_intcategoryTag
{
    
  /*  if(index > 4)
    {
        index = 0;
        currentIndex=0;
     
        CGRect RectimageScroll=CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height);//ORIGIN_Y//-(ORIGIN_Y+k_ContantHeight)
        _catagoryScrollerView.frame= RectimageScroll;
        
        _catagoryScrollerView.contentSize = CGSizeMake(_catagoryScrollerView.frame.size.width *5, _catagoryScrollerView.frame.size.height);
        
        _catagoryScrollerView.contentOffset = CGPointMake(_catagoryScrollerView.frame.size.width * index, 0);
        
    }else if(index < 0){
        index = 4;
        currentIndex=4;
        CGRect RectimageScroll=CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height);//ORIGIN_Y//-(ORIGIN_Y+k_ContantHeight)
        _catagoryScrollerView.frame= RectimageScroll;
        
        _catagoryScrollerView.contentSize = CGSizeMake(_catagoryScrollerView.frame.size.width *5, _catagoryScrollerView.frame.size.height);
        
        _catagoryScrollerView.contentOffset = CGPointMake(_catagoryScrollerView.frame.size.width * index, 0);

    }
    
    NSLog(@"%d",index);*/
    
    if(index<5){
        CustomCatagoryView *categoryView = [[CustomCatagoryView alloc] init];
        
        categoryView.frame=CGRectMake(_catagoryScrollerView.frame.size.width*index, 0, _catagoryScrollerView.frame.size.width,_catagoryScrollerView.frame.size.height);
    
           
        [categoryView  initViewWithSender:self WithIndexOfPage:index];
        
        categoryView.tag =_intcategoryTag;
        categoryView.currentPage=index;
        
        
        categoryView.autoresizingMask= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        
        [_catagoryScrollerView addSubview:categoryView];
        
        categoryView=nil;
    }
   
}


#pragma mark - Orientaion Method

-(void)resetOrientation_MediaViewer
{
    
    _catagoryScrollerView.contentSize = CGSizeMake((_catagoryScrollerView.frame.size.width * 5), _catagoryScrollerView.frame.size.height);
    
    _catagoryScrollerView.contentOffset=CGPointMake(_catagoryScrollerView.frame.size.width*currentIndex, 0);
    
    CustomCatagoryView *preCustomView=(CustomCatagoryView *)[_catagoryScrollerView viewWithTag:TAG_PREVIOUS_PAGE];
    
    [preCustomView   resetBackImage:currentIndex-1];
    
    CustomCatagoryView *currentCustomView=(CustomCatagoryView *)[_catagoryScrollerView viewWithTag:TAG_CURRENT_PAGE];
    
    [currentCustomView   resetBackImage:currentIndex];
    
    CustomCatagoryView *nextCustomView=(CustomCatagoryView *)[_catagoryScrollerView viewWithTag:TAG_NEXT_PAGE];
    
    [nextCustomView   resetBackImage:currentIndex+1];
    
   
    
}


#pragma mark Remove Previous  Views
-(void)RemovePreviousViewsAndLoadViewsWithIndexNo:(int)_indexNo
{
    
    
     UIView *removeImageViews=[_catagoryScrollerView viewWithTag:TAG_NEXT_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        [removeImageViews removeFromSuperview];
        
        
    }
    
    
    removeImageViews=[_catagoryScrollerView viewWithTag:TAG_CURRENT_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        
        [removeImageViews removeFromSuperview];
        
        
    }
    
    
   removeImageViews=[_catagoryScrollerView viewWithTag:TAG_PREVIOUS_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        
        [removeImageViews removeFromSuperview];
        
    }
    
   
    [self LoadScrollviewWithCategory:_indexNo-1 CategoryTag:TAG_PREVIOUS_PAGE];
    [self  resetOrientation_MediaViewer];
    
    [self LoadScrollviewWithCategory:_indexNo CategoryTag:TAG_CURRENT_PAGE];
    [self  resetOrientation_MediaViewer];
    
    [self LoadScrollviewWithCategory:_indexNo+1 CategoryTag:TAG_NEXT_PAGE];
    [self  resetOrientation_MediaViewer];
    
   
}


#pragma mark-
#pragma mark  Move To category

-(void)moveToNextCategory:(int)index
{
    
    int page=(index+1);
    
    if(page>=5){
        
        return;
    }
    else{
        
        if(_catagoryScrollerView.tag == kCATEGORY_SCROLLER_TAG)
        {
            if(page!= currentIndex)
            {
                
                currentIndex=page;
                
                //[self RemovePreviousViewsAndLoadViewsWithIndexNo:page];
                
                [self  slideViewsWithAnimation:page];
                
            }
        }
    }
}

-(void)moveToPreviousCategory:(int)index
{
    
    
    int page=(index-1);
    
    if(page<0){
        
        return;
    
    }else{
        
        if(_catagoryScrollerView.tag == kCATEGORY_SCROLLER_TAG)
        {
            if(page!= currentIndex)
            {
                
                currentIndex=page;
                
               // [self RemovePreviousViewsAndLoadViewsWithIndexNo:page];
                 [self  slideViewsWithAnimation:page];
                
            }
        }
    }
}


#pragma mark-
#pragma mark  Select Category

-(void)didClickOnSelectedCategory:(int)index
{
    switch (index) {
        case kWHOS_PALYING:
        {
            [self performSegueWithIdentifier:kWHOS_PLAYING_VC sender: self];
        }
            break;
        case kWHOS_DRINKING:
        {
            [self performSegueWithIdentifier:kWHOS_DRINKING_VC sender: self];
        
        }
            break;
        case kWHOS_PARTYING:
        {
            [self performSegueWithIdentifier:kWHOS_PARTYING_VC sender: self];
            
        }
            break;
        case kWHOS_EATING:
        {
            [self performSegueWithIdentifier:kWHOS_EATING_VC sender: self];
            
        }
            break;
        case kWHOS_WATCHING:
        {
            [self performSegueWithIdentifier:kWHOS_WATCHING_VC sender: self];
            
        }
            break;
        default:
            break;
    }

}

#pragma mark-
#pragma mark  Prepare For Segue Action With Identifire

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kWHOS_PLAYING_VC]) {
         Who_sPlayingViewController *playingVC = [segue destinationViewController];
         playingVC.indexPage=currentIndex;
        
    }else  if ([[segue identifier] isEqualToString:kWHOS_DRINKING_VC]) {
        Who_sDrinkingViewController *drinkingVC = [segue destinationViewController];
        drinkingVC.indexPage=currentIndex;
        
    }
    else  if ([[segue identifier] isEqualToString:kWHOS_PARTYING_VC]) {
        Who_sPartyingViewController *partyingVC = [segue destinationViewController];
        partyingVC.indexPage=currentIndex;
        
    }
    else  if ([[segue identifier] isEqualToString:kWHOS_EATING_VC]) {
        Who_sEatingViewController *eatingVC = [segue destinationViewController];
        eatingVC.indexPage=currentIndex;
        
    }
    else  if ([[segue identifier] isEqualToString:kWHOS_WATCHING_VC]) {
        Who_sWatchingViewController *watchingVC = [segue destinationViewController];
        watchingVC.indexPage=currentIndex;
        
    }
}


#pragma mark-
#pragma mark  Slide view with animation

-(void)slideViewsWithAnimation:(int)_page
{
    UIView *removeImageViews=[_catagoryScrollerView viewWithTag:TAG_NEXT_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        [removeImageViews removeFromSuperview];
        
        
    }
    removeImageViews=[_catagoryScrollerView viewWithTag:TAG_CURRENT_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        
        [removeImageViews removeFromSuperview];
        
        
    }
    
    removeImageViews=[_catagoryScrollerView viewWithTag:TAG_PREVIOUS_PAGE];
    
    
    if(removeImageViews)
    {
        //NSLog(@"----------%d",removeImageViews.tag);
        
        [removeImageViews removeFromSuperview];
        
    }
    
    
    
    [self LoadScrollviewWithCategoryWithAnimation:_page-1 CategoryTag:TAG_PREVIOUS_PAGE];
    [self  resetOrientation_MediaViewer];
    [self LoadScrollviewWithCategoryWithAnimation:_page CategoryTag:TAG_CURRENT_PAGE];
    [self  resetOrientation_MediaViewer];
    [self LoadScrollviewWithCategoryWithAnimation:_page+1 CategoryTag:TAG_NEXT_PAGE];
    [self  resetOrientation_MediaViewer];

}


-(void)LoadScrollviewWithCategoryWithAnimation:(int)pageNumber CategoryTag:(int)_categoryTag
{
    
    if(pageNumber < 0)
        return;
    
    [self displayNewCategoryWithAnimation:pageNumber CategoryTag:_categoryTag];
}



#pragma mark-
#pragma mark  Display New slide View  with animation

-(void)displayNewCategoryWithAnimation:(int)index CategoryTag:(int)_intcategoryTag
{
    
    if(index < 5)
    {
        CustomCatagoryView *categoryView = [[CustomCatagoryView alloc] init];
        
        categoryView.frame=CGRectMake(_catagoryScrollerView.frame.size.width*index, 0, _catagoryScrollerView.bounds.size.width,_catagoryScrollerView.bounds.size.height);
        
        [categoryView  initViewWithSender:self WithIndexOfPage:index];
        
        categoryView.tag =_intcategoryTag;
        categoryView.currentPage=index;
        
        
        categoryView.autoresizingMask= UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
       
         categoryView.alpha = 0.0;
        [UIView  transitionWithView:categoryView duration:0.6 options:UIViewAnimationOptionCurveLinear  animations:^{
            
             categoryView.alpha = 1.0;
        } completion:^(BOOL finished) {
           
        }];
        
        [_catagoryScrollerView addSubview:categoryView];
        
        categoryView=nil;
        
    }
}




@end
