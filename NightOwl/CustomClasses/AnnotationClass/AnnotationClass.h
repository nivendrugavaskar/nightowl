//
//  AnnotationClass.h
//  NightOwl
//
//  Created by Subhra_Limtex on 5/9/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface AnnotationClass : NSObject<MKAnnotation>

@property (nonatomic, weak) NSString *title;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithTitle:(NSString *)Loctitle   AndCoordinate:(CLLocationCoordinate2D)coOrdinate2d;


@end
