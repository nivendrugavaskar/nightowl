//
//  Common.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Common <NSObject>

#define   kLEFTBTN_TAG                      1000
#define   kRIGHTBTN_TAG                     1001
#define   kCATAGORYSELECTION_BTN_TAG        1002 

#define   kCATAGORYBTN_WIDTH                150
#define   kCATAGORYBTN_HEIGHT               100

#define kORIGIN_X                           0.0
#define kORIGIN_Y                           0.0

#define kCATEGORY_SCROLLER_TAG              100
#define TAG_PREVIOUS_PAGE                   10011
#define TAG_CURRENT_PAGE                    10022
#define TAG_NEXT_PAGE                       10033
#define  k_ContantHeight                    48

#define  kIMGEVIEW_TAG                      1003

#define  TAG_THIRD_PAGE                     1004
#define  TAG_FOURTH_PAGE                    1005
#define  TAG_FIFTH_PAGE                     1006



#define    kWHOS_PALYING                        0
#define    kWHOS_DRINKING                       1
#define    kWHOS_PARTYING                       2
#define    kWHOS_EATING                         3
#define    kWHOS_WATCHING                       4

#define    kWHOS_PLAYING_VC                     @"Who_sPlayingVC"
#define    kWHOS_DRINKING_VC                    @"Who_sDrinkingVC"
#define    kWHOS_PARTYING_VC                    @"Who_sPartyingVC"
#define    kWHOS_EATING_VC                      @"Who_sEatingVC"
#define    kWHOS_WATCHING_VC                    @"Who_sWatchingVC"

#define    kListViewControllerEat                  @"ListOfSubcategoryEatVC"
#define    kListViewControllerPlay                 @"ListOfSubcategoryPlayVC"
#define    kListViewControllerDrink                @"ListOfSubcategoryDrinkVC"
#define    kListViewControllerParty                @"ListOfSubcategoryPartyVC"
#define    kListViewControllerWatch                @"ListOfSubcategoryWatchVC"

#define    kPlaceDetailsVC                         @"PlaceDetailsVC"

#define    kFONT_SIZE                           14

#define kGOOGLE_API_KEY                         @"AIzaSyDJbFnbZjeozOsZIKCoTcNCXtoN61Nq7J8"
#define kFILTER_DISTANCE                        @"15000"

#define  DOC_Path(path)    [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]  stringByAppendingPathComponent:path]

#define  kAPP_ALERT_TITLE     @"Night Owl"

//----------Alert View Text--------------//

#define   kLIST_NOT_FOUND       @"No List Found!"
//--------------------------//

#define     kBASE_URL    @"http://dev.businessprodemo.com/night_owl/index.php?"

#define ALT_MSG_NOT_CONFIGURED_DEVICE @"Device not configured to send Mail."
#define ALT_MMS_NOT_CONFIGURED_DEVICE @"Device not configured to send Message."

#define ALERT_TAG_FAILED_TO_SEND 13331
#define ALERT_TAG_MAIL_NOT_SENT 13332
#define ALERT_TAG_SUCCESS_SEND 13334

#define MAILSUBJECT @"Night Owl"
#define MAIL_BODY   @"Hi, I wanted to send you the contact.  It is a great app for searching your hot spot!"

#define   ALERT_TAG_MAIL_FAIL           23331
#define   ALERT_TAG_MAIL_SAVED          23332

#define   kHOT_SOPT_IMAGE               @"HotSpot.jpg"

#define   APPLICATION_ID                @"231836723602503"
//#define   kFACEBOOKSHARING_TEXT         @""


@end
