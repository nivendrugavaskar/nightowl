//
//  CellDesigner.h

//


#import <Foundation/Foundation.h>

@class PlayingCell,CustomCell;


@interface CellDesigner : NSObject

+ (UITableViewCell *) getTableViewCellFromNib:(NSString *)nibName;

+ (PlayingCell *) getPlayingCellForTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath nibName:(NSString*)nibname;


+ (CustomCell *) getCustomCellForTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath nibName:(NSString*)nibname;

@end
