//
//  CellDesigner.m

//

#import "CellDesigner.h"

@implementation CellDesigner


+ (UITableViewCell *) getTableViewCellFromNib:(NSString *)nibName{
	NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
	for (id currentObject in topLevelObjects) {
		if ([currentObject isKindOfClass:[UITableViewCell class]]) {
			return (UITableViewCell *)currentObject;
		}
	}
	return nil;
}


+ (PlayingCell *) getPlayingCellForTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath nibName:(NSString*)nibname{
    
    PlayingCell *cell = (PlayingCell *)[tableView dequeueReusableCellWithIdentifier:@"Playing"];
    if (cell == nil) {
        cell = (PlayingCell *)[CellDesigner getTableViewCellFromNib:nibname];
    }
    return cell;
    
}

+ (CustomCell *) getCustomCellForTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath nibName:(NSString*)nibname
{
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:@"Custom"];
    if (cell == nil) {
        cell = (CustomCell *)[CellDesigner getTableViewCellFromNib:nibname];
    }
    return cell;

}



@end
