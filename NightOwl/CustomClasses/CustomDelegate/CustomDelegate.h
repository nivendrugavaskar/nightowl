//
//  CustomDelegate.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/10/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CustomDelegate <NSObject>

@optional

-(void)loadNearestPlaces;
-(void)loadListForRetrivedData:(NSMutableArray*)listArr;
-(void)hidesLoadingView;

-(void)showLoadingForFaceBook;
-(void)hidesLoadingForFaceBook;

@end
