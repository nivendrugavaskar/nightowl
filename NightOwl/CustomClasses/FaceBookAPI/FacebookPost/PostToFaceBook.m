//
//  PostToFaceBook.m
//  FBShare
//
//  Created by Mac Mini 2 on 02/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//




#import "PostToFaceBook.h"
#import "NightOwlAppDelegate.h"
#import "CustomAlertView.h"

@implementation NSDictionary (Utility)

- (id)objectCheckForKeyNotNull:(id)key {
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return @"";
    
    return object;
}
@end
@implementation PostToFaceBook

@synthesize isFacebookLogedin;
@synthesize facebook =_facebook;
@synthesize morphVideoPath;

//@synthesize Parent;
NSString *postType;
UIImage  *shareImage;

//BOOL checked = NO;
-(void)registerViaFacebook
{
    if(self.facebook==nil){
        self.facebook = [[Facebook alloc] init];
        
    }
    
	arr_FB_Permissions =  [NSArray arrayWithObjects: @"publish_stream", @"read_stream", @"offline_access", @"user_birthday", @"friends_birthday", @"friends_work_history", @"friends_hometown", @"email",@"user_likes",@"friends_likes",@"offline_access",@"publish_checkins",@"read_mailbox",@"user_work_history",@"user_checkins",@"user_about_me",nil];

	[_facebook authorize:APPLICATION_ID permissions:arr_FB_Permissions delegate:self];
    
}

-(void)postToWall:(NSString*)text
{
    loginForAd =YES;
    Counter=0;
    
    sharingTxt=text;
    
    //if(_delegate)
       // [_delegate  showLoadingForFaceBook];
    
    //checked = YES;
    postType=@"link";
    if([_facebook isSessionValid])
    {  
        [self sendPostLink:nil withText:text];
    }
    else
    {
        flagForFbRequest=0;
        [self registerViaFacebook];
    }
}

-(void)sendPostLink :(UIImage*)postImage withText:(NSString*)postText
{
    
    flagForFbRequest=1;
    
  
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:postText forKey:@"message"];
    //[params setObject:@"link" forKey:@"type"];
    //[params setObject:kiTUNES_LINK forKey:@"link"];
    //[params setObject:@"Link description" forKey:@"description"];
    [params  setObject:@"NightOwl.."  forKey:@"name"];
    [params  setObject:@"I downloaded this App and thought you might like it too" forKey:@"caption"];
    
    
   
    
    [_facebook requestWithGraphPath:@"me/feed"
                          andParams:params
                      andHttpMethod:@"POST"
                        andDelegate:self];
    
}



-(void)uploadImage:(UIImage*)image
{
    postType=@"image";
    if([_facebook isSessionValid])
    {  
        [self photoUpload:image];
    }
    else
    {
        flagForFbRequest=0;
        shareImage=image;
        [self registerViaFacebook];
    }

}
-(void)loginToFacebook
{
    loginForAd =NO;
    
    appDelegate=(NightOwlAppDelegate *)[[UIApplication sharedApplication] delegate];
    if([_facebook isSessionValid])
    {  
        return;
    }
    else
    {
        flagForFbRequest=0;
        
        [self registerViaFacebook];
    }

}

-(void)photoUpload:(UIImage *)image
{
    
    flagForFbRequest=1;
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:image forKey:@"image"];  
    
    [_facebook requestWithMethodName:@"photos.upload" andParams:params andHttpMethod:@"POST" andDelegate:self];
    
}
-(void) fbDidLogin{
	
    is_facebookData_loaded = YES;
    flagForFbRequest=0;
    
    if(loginForAd){
        
        NSLog(@"%@",sharingTxt);
        
        [_facebook requestWithGraphPath:@"me" andDelegate:self];
        [self sendPostLink:nil withText:sharingTxt];
    }
    else{
        
        NSData *videoData = [NSData dataWithContentsOfFile:morphVideoPath];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       videoData, @"TimeScape.mp4",
                                       @"video/mp4", @"contentType",
                                       @"TimeScape", @"TimeScape",
                                       nil];  //@"Video Test Description", @"description",
        [_facebook requestWithGraphPath:@"me/videos"
                              andParams:params
                          andHttpMethod:@"POST"
                            andDelegate:self];
    }
	
    
}

-(void) fbDidLogout{
    NSLog(@"Logged out of facebook");
    _facebook.accessToken = nil;
    _facebook.expirationDate = nil;
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }

}
/*
 --------------------------------------------------------------------------------------------------------------------------------------------
 REQUESTING DELEGATE STARTED.
 --------------------------------------------------------------------------------------------------------------------------------------------
 */

- (void)request:(FBRequest*)request didReceiveResponse:(NSURLResponse*)response	{
    
    loginForAd=NO;

}

- (void)request:(FBRequest*)request didFailWithError:(NSError*)error{
    if(flagForFbRequest==1)
    {
        
        CustomAlertView   *alert=[[CustomAlertView  alloc]  initWithTitle:kAPP_ALERT_TITLE message:@"Post Unsuccessfull!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert   show];

    }
}

- (void)request:(FBRequest*)request didLoad:(id)result{
	if (is_facebookData_loaded == YES){
        
        is_facebookData_loaded = NO;
    
		NSMutableDictionary *DCT_Details = [NSMutableDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat: @"select uid, name, sex, email, contact_email, birthday_date,profile_url,first_name,last_name,current_location,pic_small,pic_square from user where uid == %@", [result valueForKey:@"id"]], @"query", nil];
        
		[_facebook requestWithMethodName:@"fql.query" andParams:DCT_Details andHttpMethod:@"POST" andDelegate:self];
        

        
		
	}else{
        
		//arr_FB_Query_Result = result;//User details
      
      
      isFacebookLogedin=NO;
      [self   fbDidLogout];
      
      if(Counter==0)
      {
          Counter=1;
          
      }
      else if(Counter==1)
      {
          
          Counter=0;
          
          return;
      }
      
      
      CustomAlertView   *alert=[[CustomAlertView  alloc]  initWithTitle:kAPP_ALERT_TITLE message:@"Post Successfull!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
      
      [alert   show];
       
        
        if(flagForFbRequest==0)
       {
        ///////login process complete,,,,,store user details in dictionary/////
           
       }
        else if(flagForFbRequest==1)
        {

            shareImage=nil;

            NSLog(@"Face Book Post completted");
            
        }
		
	}
}
- (void)dialogDidComplete:(FBDialog *)dialog
{
    CustomAlertView   *alert=[[CustomAlertView  alloc]  initWithTitle:kAPP_ALERT_TITLE message:@"Post Successfull!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert   show];

    
}
-(void)fbDidNotLogin:(BOOL)cancelled
{
    
}

#pragma mark-
#pragma mark   Hide loading View delegate method

-(void)hidesLoadingForFacebook
{
   // if(_delegate)
      //  [_delegate   hidesLoadingForFaceBook];
}


@end
