//
//  PostToFaceBook.h
//  FBShare
//
//  Created by Mac Mini 2 on 02/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Facebook.h"
#import "JSON.h"
#import "CustomDelegate.h"
 

@class NightOwlAppDelegate,SelectedMorphOptionsViewController,SMMasterViewController,EditSequenceViewController;

@interface PostToFaceBook : NSObject<FBRequestDelegate,FBDialogDelegate,FBSessionDelegate,UIAlertViewDelegate,CustomDelegate>
{
    Facebook *_facebook;
    BOOL isFacebookLogedin;
    BOOL is_facebookData_loaded;
    NSMutableArray *arr_FB_Permissions;
    NSMutableArray *arr_FB_Query_Result;
    
    int flagForFbRequest;
    
    //id Parent;
    NightOwlAppDelegate *appDelegate;
    
    NSString     *morphVideoPath;
    
    BOOL   loginForAd;
    
    int  Counter;
    
    NSString   *sharingTxt;
}

@property(nonatomic,readwrite)BOOL isFacebookLogedin;
@property(nonatomic,retain)Facebook *facebook;
@property(nonatomic,strong)id  Parent;
@property(nonatomic,strong)NSString     *morphVideoPath;

@property(nonatomic,strong)id<CustomDelegate> delegate;

-(void)registerViaFacebook;
-(void)sendPostLink :(UIImage*)postImage withText:(NSString*)postText;
-(void)postToWall:(NSString*)text;

-(void)uploadImage:(UIImage *)image;
-(void)photoUpload:(UIImage *)image;
-(void)loginToFacebook;


@end
