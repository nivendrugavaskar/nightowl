//
//  InitCustomLocation.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/9/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "InitCustomLocation.h"
#import "NightOwlAppDelegate.h"

@implementation InitCustomLocation


static    InitCustomLocation    *shareObject=nil;

#pragma mark-
#pragma mark   Share Instance

+(InitCustomLocation*)shareInstance
{
    if(shareObject==nil)
    {
        shareObject=[[InitCustomLocation  alloc]  init];
    }
    
    return shareObject;
}


-(void)initCustomLocation
{
    if(!appDel)
        appDel=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    [self  loadCurrentLocation];
}

-(void)loadCurrentLocation
{
    
    if(locationManager)
    {
        
        [locationManager  setDelegate:nil];
        locationManager=nil;
    }
    
    if(!locationManager){
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate =(id)self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = 5000.0f;
    }
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
//fromLocation:(CLLocation *)oldLocation
//{
//    
//    appDel.currentLocation=newLocation;
//    
//    if(_delegate)
//        [_delegate  loadNearestPlaces];
//}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
    
    appDel.currentLocation=[locations lastObject];
    
    if(_delegate)
        [_delegate  loadNearestPlaces];
}

// new code
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    // We only need to start updating location for iOS 8 -- iOS 7 users should have already
    // started getting location updates
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [manager startUpdatingLocation];
    }
}


@end
