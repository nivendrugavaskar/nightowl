//
//  InitCustomLocation.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/9/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomDelegate.h"

@class NightOwlAppDelegate;

@interface InitCustomLocation : NSObject<CLLocationManagerDelegate,CustomDelegate>
{
    NightOwlAppDelegate   *appDel;
    CLLocationCoordinate2D currentCentre;
     CLLocationManager *locationManager;
}

+(InitCustomLocation*)shareInstance;

-(void)initCustomLocation;
-(void)loadCurrentLocation;

@property(nonatomic,strong)id<CustomDelegate> delegate;


@end
