//
//  CustomLoadingView.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/8/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "CustomLoadingView.h"

@implementation CustomLoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark-
#pragma mark  Init Loading View

-(void)initLoadingView
{
    _holderView.layer.cornerRadius=4.0;
}


#pragma mark-
#pragma mark  Start Loading

-(void)startLoading
{
    [UIView  beginAnimations:@"" context:NULL];
    [UIView  setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView  setAnimationDelay:0.5];
    self.alpha=1.0;
    
    [_activity  startAnimating];
    
    [UIView  commitAnimations];

}

#pragma mark-
#pragma mark  Start Loading

-(void)stopLoading
{
    [UIView  beginAnimations:@"" context:NULL];
    [UIView  setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView  setAnimationDelay:0.5];
    self.alpha=0.0;
    
    [_activity  stopAnimating];
    
    [UIView  commitAnimations];
}




@end
