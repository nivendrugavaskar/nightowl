//
//  DataFetchProcess.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/26/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomDelegate.h"


@interface DataFetchProcess : NSObject<CustomDelegate>
{

    NSMutableArray    *retrivedDataArr;
}

@property(nonatomic,weak)id<CustomDelegate>delegate;


+(DataFetchProcess*)shareInstance;
-(void)initializeDataFetchProcess: (NSString*)fetchURL :(NSString *)parameter;

-(void)fetchDataWithURL :(NSString*)URL :(NSString *)post;

-(void)getDataFromURL:(NSData*)serviceData;

@end
