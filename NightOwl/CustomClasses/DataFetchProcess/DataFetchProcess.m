//
//  DataFetchProcess.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/26/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "DataFetchProcess.h"
#import "JSON.h"
#import "CustomAlertView.h"

@implementation DataFetchProcess


static   DataFetchProcess    *shareObject=nil;

#pragma mark-
#pragma mark  Share Instance

+(DataFetchProcess*)shareInstance
{
    if(shareObject==nil){
    
        shareObject=[[DataFetchProcess  alloc]  init];
    }
    
    return shareObject;

}


#pragma mark-
#pragma mark  Initialize process

-(void)initializeDataFetchProcess :(NSString*)fetchURL :(NSString *)parameter
{
    [self  fetchDataWithURL:fetchURL:parameter];
}


#pragma mark-
#pragma mark  Fetch Data process

-(void)fetchDataWithURL :(NSString*)URL :(NSString *)post
{
    NSURL    *fetchURL=[NSURL  URLWithString:URL];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:fetchURL];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection  sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([data length] > 0 && error == nil)
        {
           
            [self  performSelectorOnMainThread:@selector(getDataFromURL:) withObject:data waitUntilDone:NO];
                      
        }
        else if ([data length] == 0 && error == nil)
        {
            if(_delegate)
                [_delegate  hidesLoadingView];
            
            CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                             message:kLIST_NOT_FOUND
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil,nil];
            [customAlertView show];
            customAlertView=nil;
        }
        else if (error != nil && error.code == NSURLErrorTimedOut) //used this NSURLErrorTimedOut from foundation error responses
        {
            NSLog(@"Connection timed out: %@", error);
            
            if(_delegate)
                [_delegate  hidesLoadingView];
            
            CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                             message:@"Connection TimeOut!"
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil,nil];
            [customAlertView show];
            customAlertView=nil;
            
        }
        else if (error != nil)
        {
            NSLog(@"Problem fetching data: %@", error);
            
            if(_delegate)
                [_delegate  hidesLoadingView];
            
            CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                             message:@"Error in Connection!"
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil,nil];
            [customAlertView show];
            customAlertView=nil;
            
        }
        
    }];


}

#pragma mark-
#pragma mark  get Data process

-(void)getDataFromURL:(NSData*)serviceData
{

    NSString *retVal=[[NSString alloc] initWithData:serviceData encoding:NSUTF8StringEncoding];
    NSLog(@"retval: %@", retVal);
    
    retrivedDataArr =[[retVal  JSONValue]   valueForKey:@"data"];
    
    NSLog(@"%@",retrivedDataArr);
    
    if(_delegate)
        [_delegate  loadListForRetrivedData:retrivedDataArr];
    
    if(_delegate)
        [_delegate  hidesLoadingView];
}




@end
