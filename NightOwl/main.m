//
//  main.m
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NightOwlAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NightOwlAppDelegate class]));
    }
}
