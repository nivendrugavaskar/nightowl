//
//  NightOwlAppDelegate.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PostToFaceBook.h"

@interface NightOwlAppDelegate : UIResponder <UIApplicationDelegate>
{

    UIImageView    *splashView;
   
    
}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic,strong)NSMutableArray    *catagoryArr;
//@property(nonatomic,readwrite)UIInterfaceOrientation   orientation;
@property(nonatomic,readwrite)UIInterfaceOrientation   orientation;
@property(nonatomic,strong)NSMutableArray      *backImagePotraitArr;
@property(nonatomic,strong)NSMutableArray      *backImageLandScapeArr;

@property(nonatomic,readwrite)int          isLandScape;

@property(nonatomic,strong)CLLocation   *currentLocation;
@property(nonatomic,readwrite)float    currentlatitude;
@property(nonatomic,readwrite)float    currentLogitude;

@property(nonatomic,strong)NSMutableArray    *backImageDetailPotraitArr;
@property(nonatomic,strong)NSMutableArray    *backImageDetailLandScapeArr;

@property(nonatomic,strong)PostToFaceBook *posttoFB;


@end
