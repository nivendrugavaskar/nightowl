//
//  Who_sPartyingViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/22/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDelegate.h"
#import "DataFetchProcess.h"
#import "CustomLoadingView.h"

@class NightOwlAppDelegate;

@interface Who_sPartyingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomDelegate>
{
    NightOwlAppDelegate    *appDelegate;
    NSMutableArray         *partyCategoryArr;
    
    int  selectCategoryParty;
}


@property (weak, nonatomic) IBOutlet UIImageView *partyBackImage;

- (IBAction)didTapOnBackHome:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *partyingTable;

@property(nonatomic,readwrite)int  indexPage;

-(void)resetOrientation_ForWhosPartying;

-(void)showLoadingViewForPartying;

@property (strong, nonatomic) IBOutlet CustomLoadingView *loadingForPartying;


@end
