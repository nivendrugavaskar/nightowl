//
//  Who_sEatingViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/22/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDelegate.h"
#import "DataFetchProcess.h"
#import "CustomLoadingView.h"

@class NightOwlAppDelegate;

@interface Who_sEatingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomDelegate>
{
    NightOwlAppDelegate    *appDelegate;
    
    NSMutableArray     *eatingCategoryArr;
    
    int   selectCategory;
}

@property (weak, nonatomic) IBOutlet UITableView *eatingTable;

- (IBAction)didTapBackFromEating:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *eatingBackImage;

@property(nonatomic,readwrite)int  indexPage;

-(void)resetOrientation_ForWhosEating;
-(void)showLoadingViewForEating;

@property (strong, nonatomic) IBOutlet CustomLoadingView *loadingForEating;


@end
