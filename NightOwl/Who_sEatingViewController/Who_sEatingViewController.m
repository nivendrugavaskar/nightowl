//
//  Who_sEatingViewController.m
//  NightOwl
//
//  Created by Subhra_Limtex on 3/22/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "Who_sEatingViewController.h"
#import "CellDesigner.h"
#import "PlayingCell.h"
#import "NightOwlAppDelegate.h"
#import "ListOfEachCatagoryViewController.h"
#import "CustomAlertView.h"

@interface Who_sEatingViewController ()

@end

@implementation UINavigationController (rotation)
//temp hack for iOS6, this allows passing supportedInterfaceOrientations to child viewcontrollers.
- (NSUInteger)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation Who_sEatingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark-
#pragma mark  Show Loading

-(void)showLoadingViewForEating
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _loadingForEating.alpha=1.0;
    [_loadingForEating  startLoading];
    
    [UIView   commitAnimations];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
     appDelegate=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    
    [self.view  addSubview:_loadingForEating];
    [_loadingForEating  initLoadingView];
    _loadingForEating.alpha=0.0;
    
    // new code
    _loadingForEating.holderView.center=CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    [self  resetOrientation_ForWhosEating];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super  viewWillAppear:animated];
    
    [self  showLoadingViewForEating];
    
     NSString   *urlString=[NSString  stringWithFormat:@"%@r=api/subcatlist",kBASE_URL];
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"\n" withString:@"%20"];
    
    NSString *post = [NSString stringWithFormat:@"id=%d",5];
    
    [[DataFetchProcess  shareInstance] initializeDataFetchProcess :urlString :post];
    [[DataFetchProcess  shareInstance]  setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setEatingBackImage:nil];
    [self setEatingTable:nil];
    [self setLoadingForEating:nil];
    [super viewDidUnload];
}
#pragma mark-
#pragma mark  Did back to Home

- (IBAction)didTapBackFromEating:(id)sender {
    
    if(_loadingForEating){
        [_loadingForEating  removeFromSuperview];
    }
    
    if([DataFetchProcess  shareInstance])
    {
        [[DataFetchProcess  shareInstance]  setDelegate:nil];
    }
    
    [self.navigationController  popViewControllerAnimated:YES];
}


#pragma mark-
#pragma mark  Orientation;


#ifdef  __IPHONE_6_0

-(BOOL)shouldAutorotate
{
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosEating];
    return   UIInterfaceOrientationMaskAll;
    
}

#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosEating];
    
    return YES;
    
    
}



-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosEating];
}


-(void)resetOrientation_ForWhosEating
{
    if(appDelegate.orientation==UIInterfaceOrientationLandscapeLeft || appDelegate.orientation==UIInterfaceOrientationLandscapeRight){
        
        [_eatingBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailLandScapeArr  objectAtIndex:_indexPage]]];
        
        appDelegate.isLandScape=1;
    }
    else if(appDelegate.orientation==UIInterfaceOrientationPortraitUpsideDown){
        
        [_eatingBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexPage]]];
        
    }
    else{
        
        [_eatingBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexPage]]];
        
    }
    
}



#pragma mark-
#pragma mark  Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [eatingCategoryArr  count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    cell=(PlayingCell*)[CellDesigner  getPlayingCellForTable:tableView indexPath:indexPath nibName:@"PlayingCell"];
    

    [(PlayingCell*)cell   setPlayingCellDetails:[eatingCategoryArr  objectAtIndex:indexPath.row]];
    
    
    return cell;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView   *footerView=[[UIView  alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
    footerView.backgroundColor=[UIColor  clearColor];
    
    return footerView;
    
}


#pragma mark-
#pragma mark  Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectCategory=indexPath.row;
    
    [self performSegueWithIdentifier:kListViewControllerEat sender: self];
}


#pragma mark-
#pragma mark  Prepare For Segue Action With Identifire

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kListViewControllerEat]) {
        ListOfEachCatagoryViewController *listVC = [segue destinationViewController];
        listVC.indexPage=_indexPage;
        
        NSMutableDictionary   *categoryDic=[eatingCategoryArr  objectAtIndex:selectCategory];
        
        NSLog(@"%@",[categoryDic  valueForKey:@"SelectedText"]);
        listVC.categoryName=[categoryDic  valueForKey:@"SelectedText"];
        
    }
}


#pragma mark -
#pragma mark  Custom Delegate Methods

#pragma mark  load retrived data

-(void)loadListForRetrivedData:(NSMutableArray*)listArr
{
    eatingCategoryArr=listArr;
    
    if([eatingCategoryArr  count]>0)
    {
        [_eatingTable  reloadData];
    }else{
        
        CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                         message:kLIST_NOT_FOUND
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil,nil];
        [customAlertView show];
        customAlertView=nil;
    }
    
}

#pragma mark  Hides Loading View

-(void)hidesLoadingView
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _loadingForEating.alpha=0.0;
    [_loadingForEating  stopLoading];
    
    [UIView   commitAnimations];
}

#pragma mark-
#pragma mark  Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){
        
        if([DataFetchProcess  shareInstance])
            [[DataFetchProcess  shareInstance]  setDelegate:nil];
        
        if(_loadingForEating)
            [_loadingForEating  removeFromSuperview];
        
        [self.navigationController  popViewControllerAnimated:YES];
    }
}



@end
