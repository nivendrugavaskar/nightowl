//
//  CustomCell.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/8/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)initialiseCell
{
    _iconImage.layer.cornerRadius=4.0;
    _iconImage.layer.borderColor=[UIColor  whiteColor].CGColor;
    _iconImage.layer.borderWidth=2.0;
    
    [_iconImage  setClipsToBounds:YES];
    
    _lblPlaceName.font=[UIFont  fontWithName:@"Cookies" size:kFONT_SIZE];
    
    _customRateView.notSelectedImage = [UIImage imageNamed:@"no_star.png"];
    _customRateView.halfSelectedImage = [UIImage imageNamed:@"half_star.png"];
    _customRateView.fullSelectedImage = [UIImage imageNamed:@"full_star.png"];
    _customRateView.rating = 0;
    _customRateView.editable = NO;
    _customRateView.maxRating = 5;
    
   
}



#pragma mark-
#pragma mark  LoadList Cell


-(void)loadListCell:(NSMutableDictionary*)placeDic
{
    
    [_activity  setHidden:YES];
    
    _lblPlaceName.text=[placeDic  valueForKey:@"p_name"];
    
    NSString   *iconURL=[[placeDic  valueForKey:@"p_image"] lastObject];
    
    iconName=[iconURL  lastPathComponent];
    
    if([self  checkRateValue:[placeDic  valueForKey:@"rat_value"]])
    {
        _customRateView.rating=[[placeDic  valueForKey:@"rat_value"] floatValue];
    }
    
       
    if([[NSFileManager defaultManager]  fileExistsAtPath:DOC_Path(iconName)])
    {
        
        if([UIImage  imageWithContentsOfFile:DOC_Path(iconName)])
            [_iconImage  setImage:[UIImage  imageWithContentsOfFile:DOC_Path(iconName)]];
        
    }
    else{
        
        [_activity  setHidden:NO];
        [_activity  startAnimating];
        
        NSURL    *imageURl=[NSURL  URLWithString:iconURL];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:imageURl];
        [request setHTTPMethod:@"GET"];
        
        NSOperationQueue *queue = [NSOperationQueue mainQueue];
        [NSURLConnection  sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            if ([data length] > 0 && error == nil)
            {
                
                [self  performSelectorOnMainThread:@selector(loadImageForList:) withObject:data waitUntilDone:NO];
            }
            else if ([data length] == 0 && error == nil)
            {
                
                [_activity  stopAnimating];
                [_activity  setHidden:YES];
                
                [_iconImage  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
            }
            else if (error != nil && error.code == NSURLErrorTimedOut) //used this NSURLErrorTimedOut from foundation error responses
            {
                NSLog(@"Connection timed out: %@", error);
                
                [_activity  stopAnimating];
                [_activity  setHidden:YES];
                
                [_iconImage  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
            }
            else if (error != nil)
            {
                NSLog(@"Problem fetching data: %@", error);
                
                [_activity  stopAnimating];
                [_activity  setHidden:YES];
                
                [_iconImage  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
            }
            
            
            
        }];
        
    }
    
}


#pragma mark-
#pragma mark   Load Image

-(void)loadImageForList:(NSData*)imageData
{
    
    UIImage   *iconImg=[UIImage  imageWithData:imageData];
    
    if(iconImg){
        
        _iconImage.alpha=0.0;
        
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3f];
        
        _iconImage.alpha=1.0;
        [_iconImage  setImage:iconImg];
        
        [UIView  commitAnimations];
        
        NSData *ConvertImageData =UIImageJPEGRepresentation(iconImg, 1.0);
        
        [[NSFileManager  defaultManager]  removeItemAtPath:DOC_Path(iconName) error:nil];
        
        [[NSFileManager defaultManager] createFileAtPath:DOC_Path(iconName) contents:ConvertImageData attributes:nil];
        
        [_activity  stopAnimating];
        [_activity  setHidden:YES];
        
    }else{
        
        [_iconImage  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
        
        [_activity  stopAnimating];
        [_activity  setHidden:YES];
        
    }
    
}

-(BOOL)checkRateValue:(NSString*)rateVal
{
    if([rateVal   isKindOfClass:[NSNull  class]]){
        return NO;
    }else{
        return YES;
    }
}

@end
