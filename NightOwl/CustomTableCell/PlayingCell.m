//
//  PlayingCell.m
//  NightOwl
//
//  Created by Subhra_Limtex on 3/21/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "PlayingCell.h"

@implementation PlayingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setPlayingCellDetails:(NSMutableDictionary*)placeDic
{
    
    _playingCellIcon.layer.cornerRadius=4.0;
    _playingCellIcon.layer.borderColor=[UIColor  whiteColor].CGColor;
    _playingCellIcon.layer.borderWidth=2.0;
    
    [_playingCellIcon  setClipsToBounds:YES];
    
    
    [_indicator  setHidden:YES];
    
    _subCategoryTitle.font=[UIFont  fontWithName:@"Cookies" size:kFONT_SIZE];
    
    _subCategoryTitle.text=[placeDic  valueForKey:@"cat_name"];
    
    if([placeDic  valueForKey:@"cat_image_thumb"]){
    
        NSString   *iconURL=[placeDic  valueForKey:@"cat_image_thumb"];
        
        iconName=[iconURL  lastPathComponent];
        
        
        if([[NSFileManager defaultManager]  fileExistsAtPath:DOC_Path(iconName)])
        {
            
            if([UIImage  imageWithContentsOfFile:DOC_Path(iconName)])
                [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:DOC_Path(iconName)]];
            
        }
        else{
            
            [_indicator  setHidden:NO];
            [_indicator  startAnimating];
            
            NSURL    *imageURl=[NSURL  URLWithString:iconURL];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:imageURl];
            [request setHTTPMethod:@"GET"];
            
            NSOperationQueue *queue = [NSOperationQueue mainQueue];
            [NSURLConnection  sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                
                if ([data length] > 0 && error == nil)
                {
                    
                    [self  performSelectorOnMainThread:@selector(loadImageForList:) withObject:data waitUntilDone:NO];
                }
                else if ([data length] == 0 && error == nil)
                {
                    [_indicator  stopAnimating];
                    [_indicator  setHidden:YES];
                    
                [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                }
                else if (error != nil && error.code == NSURLErrorTimedOut) //used this NSURLErrorTimedOut from foundation error responses
                {
                    NSLog(@"Connection timed out: %@", error);
                    
                    [_indicator  stopAnimating];
                    [_indicator  setHidden:YES];
                    
                    [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                }
                else if (error != nil)
                {
                    NSLog(@"Problem fetching data: %@", error);
                    
                    [_indicator  stopAnimating];
                    [_indicator  setHidden:YES];
                    
                    [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                }
                
                
                
            }];
            
        }
    }else{
    
        [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
        
        [_indicator  stopAnimating];
        [_indicator  setHidden:YES];
    }
    
}


#pragma mark-
#pragma mark   Load Image

-(void)loadImageForList:(NSData*)imageData
{
    
    UIImage   *iconImg=[UIImage  imageWithData:imageData];
    
    if(iconImg){
        
        _playingCellIcon.alpha=0.0;
        
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3f];
        
        _playingCellIcon.alpha=1.0;
        [_playingCellIcon  setImage:iconImg];
        
        [UIView  commitAnimations];
        
        NSData *ConvertImageData =UIImageJPEGRepresentation(iconImg, 1.0);
        
        [[NSFileManager  defaultManager]  removeItemAtPath:DOC_Path(iconName) error:nil];
        
        [[NSFileManager defaultManager] createFileAtPath:DOC_Path(iconName) contents:ConvertImageData attributes:nil];
        
        [_indicator  stopAnimating];
        [_indicator  setHidden:YES];
        
    }else{
        
        [_playingCellIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
        
        [_indicator  stopAnimating];
        [_indicator  setHidden:YES];
        
    }
    
}





@end
