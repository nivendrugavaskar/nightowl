//
//  PlayingCell.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/21/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PlayingCell : UITableViewCell
{
    NSString    *iconName;
}

@property (weak, nonatomic) IBOutlet UIImageView *playingCellIcon;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;


//-(void)setImageTextWidthForCategory:(NSString*)categoryName;

//-(void)setPlayingCellDetails:(int)indexPath   IconImageName:(NSString*)imageName  SubCategoryTitle:(NSString*)titleImageName;

-(void)setPlayingCellDetails:(NSMutableDictionary*)placeDic;

@property (weak, nonatomic) IBOutlet UILabel *subCategoryTitle;

@end
