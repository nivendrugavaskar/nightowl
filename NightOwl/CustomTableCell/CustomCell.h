//
//  CustomCell.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/8/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import <QuartzCore/QuartzCore.h>

@interface CustomCell : UITableViewCell
{
    NSString   *iconName;
}

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;

@property (weak, nonatomic) IBOutlet RateView *customRateView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;


-(void)loadListCell:(NSMutableDictionary*)placeDic;
-(void)loadImageForList:(NSData*)imageData;
-(void)initialiseCell;

-(BOOL)checkRateValue:(NSString*)rateVal;

@end
