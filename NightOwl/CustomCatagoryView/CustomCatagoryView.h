//
//  CustomCatagoryView.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NightOwlAppDelegate;

@interface CustomCatagoryView : UIView
{
    id  _parentView;
    CGRect   _frame;
    
    NightOwlAppDelegate    *appDelegate;
    
    
}

@property(nonatomic,weak)UIButton     *leftNavBtn;
@property(nonatomic,weak)UIButton     *RightNavBtn;
@property(nonatomic,weak)UIButton     *catagoryNavigationBtn;

@property(nonatomic,strong)UIImageView  *backImage;
@property(nonatomic,readwrite)int   currentPage;


-(void)initViewWithSender:(id)_sender   WithIndexOfPage:(int)index;
-(void)assignCatagoryWithTitle:(NSString*)catagoryTitle;
-(void)loadSubControllersInView:(id)_sender  WithIndex:(int)_index;

-(void)navigateToCatagory:(id)sender;
-(void)navigateToNextCatagory:(id)sender;
-(void)navigateToPreviousCatagory:(id)sender;

-(void)resetBackImage:(int)indexPage;
//-(void)resetButtonFrames;

@end
