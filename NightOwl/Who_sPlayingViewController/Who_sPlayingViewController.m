//
//  Who_sPlayingViewController.m
//  NightOwl
//
//  Created by Subhra_Limtex on 3/21/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "Who_sPlayingViewController.h"
#import "NightOwlAppDelegate.h"
#import "PlayingCell.h"
#import "CellDesigner.h"
#import "ListOfEachCatagoryViewController.h"
#import "CustomAlertView.h"

@interface Who_sPlayingViewController ()

@end


@implementation UINavigationController (rotation)
//temp hack for iOS6, this allows passing supportedInterfaceOrientations to child viewcontrollers.
- (NSUInteger)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation Who_sPlayingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark-
#pragma mark  Show Loading

-(void)showLoadingViewForPlaying
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _loadingForPlaying.alpha=1.0;
    
//    // new code
//     _loadingForPlaying.center=CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    [_loadingForPlaying  startLoading];
    
    [UIView   commitAnimations];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden=YES;
    appDelegate=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    [self.view  addSubview:_loadingForPlaying];
    [_loadingForPlaying  initLoadingView];
    _loadingForPlaying.alpha=0.0;
    
    // new code
    _loadingForPlaying.holderView.center=CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    
    [self  resetOrientation_ForWhosPlaying];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super  viewWillAppear:animated];
    
    [self  showLoadingViewForPlaying];
    
   // NSData* paramsData = [@{@"key": @"value"} dataFormURLEncoded];
    
    NSString   *urlString=[NSString  stringWithFormat:@"%@r=api/subcatlist",kBASE_URL];
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"\n" withString:@"%20"];
    
    
    NSString *post = [NSString stringWithFormat:@"id=%d",2];
    
    [[DataFetchProcess  shareInstance] initializeDataFetchProcess:urlString:post];
    [[DataFetchProcess  shareInstance]  setDelegate:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setBackImage:nil];
    [self setPlayingTableView:nil];
    [self setLoadingForPlaying:nil];
    [super viewDidUnload];
}

#pragma mark-
#pragma mark  Orientation


#ifdef  __IPHONE_6_0

-(BOOL)shouldAutorotate
{
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    //UIInterfaceOrientation deviceOrientation = self.interfaceOrientation;
     UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosPlaying];
    return   UIInterfaceOrientationMaskAll;
    
}

#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
   // UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
     UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosPlaying];
    return YES;
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

   // UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
     UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForWhosPlaying];
}


-(void)resetOrientation_ForWhosPlaying
{
    if(appDelegate.orientation==UIInterfaceOrientationLandscapeLeft || appDelegate.orientation==UIInterfaceOrientationLandscapeRight){
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailLandScapeArr  objectAtIndex:_indexPage]]];
        
        appDelegate.isLandScape=1;
    }
    else if(appDelegate.orientation==UIInterfaceOrientationPortraitUpsideDown){
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr objectAtIndex:_indexPage]]];
        
    }
    else{
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr objectAtIndex:_indexPage]]];
    
    }

}

#pragma mark-
#pragma mark  Back To Home Page

- (IBAction)didBackToHome:(id)sender {
    
    if([DataFetchProcess  shareInstance])
        [[DataFetchProcess  shareInstance]  setDelegate:nil];
    
    if(_loadingForPlaying)
        [_loadingForPlaying  removeFromSuperview];
    
    [self.navigationController  popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark  Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [playCategoryArr  count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    cell=(PlayingCell*)[CellDesigner  getPlayingCellForTable:tableView indexPath:indexPath nibName:@"PlayingCell"];
    
  
    [(PlayingCell*)cell   setPlayingCellDetails:[playCategoryArr  objectAtIndex:indexPath.row]];
    
    
    return cell;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView   *footerView=[[UIView  alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
    footerView.backgroundColor=[UIColor  clearColor];
    
    return footerView;
    
}


#pragma mark-
#pragma mark  Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectCategoryPlay=indexPath.row;
    
    [self performSegueWithIdentifier:kListViewControllerPlay sender: self];
}

#pragma mark-
#pragma mark  Prepare For Segue Action With Identifire

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kListViewControllerPlay]) {
        ListOfEachCatagoryViewController *listVC = [segue destinationViewController];
        listVC.indexPage=_indexPage;
        
        NSMutableDictionary   *categoryDic=[playCategoryArr  objectAtIndex:selectCategoryPlay];
        
        NSLog(@"%@",[categoryDic  valueForKey:@"cat_name"]);
        listVC.categoryName=[categoryDic  valueForKey:@"cat_name"];
        listVC.subcategory_Id=[[categoryDic  valueForKey:@"cat_id"] integerValue];
    }
}

#pragma mark -
#pragma mark  Custom Delegate Methods

#pragma mark  load retrived data

-(void)loadListForRetrivedData:(NSMutableArray*)listArr
{
    playCategoryArr=listArr;
    
    if([playCategoryArr  count]>0)
    {
        [_playingTableView  reloadData];
        
    }else{
        
        CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                         message:kLIST_NOT_FOUND
                                                                        delegate:self
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil,nil];
        [customAlertView show];
        customAlertView=nil;
        
    }

}

#pragma mark  Hides Loading View

-(void)hidesLoadingView
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _loadingForPlaying.alpha=0.0;
    [_loadingForPlaying  stopLoading];
    
    [UIView   commitAnimations];
}

#pragma mark-
#pragma mark  Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){
        
        if([DataFetchProcess  shareInstance])
            [[DataFetchProcess  shareInstance]  setDelegate:nil];
        
        if(_loadingForPlaying)
            [_loadingForPlaying  removeFromSuperview];
        
        [self.navigationController  popViewControllerAnimated:YES];
    }
}



@end
