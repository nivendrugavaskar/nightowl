//
//  Who_sPlayingViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/21/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDelegate.h"
#import "DataFetchProcess.h"
#import "CustomLoadingView.h"

@class NightOwlAppDelegate;

@interface Who_sPlayingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,CustomDelegate>
{
    NightOwlAppDelegate    *appDelegate;
    
    NSMutableArray        *playCategoryArr;
    
    int   selectCategoryPlay;
}

@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property(nonatomic,readwrite)int  indexPage;

-(void)resetOrientation_ForWhosPlaying;

- (IBAction)didBackToHome:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *playingTableView;


@property (strong, nonatomic) IBOutlet CustomLoadingView *loadingForPlaying;

-(void)showLoadingViewForPlaying;

@end
