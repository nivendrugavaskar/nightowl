//
//  Who_sWatchingViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/22/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomDelegate.h"
#import "DataFetchProcess.h"
#import "CustomLoadingView.h"

@class NightOwlAppDelegate;

@interface Who_sWatchingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomDelegate>
{
    NightOwlAppDelegate    *appDelegate;
    
    NSMutableArray      *watchCategoryArr;
    int      selectCategoryWatch;
}

@property(nonatomic,readwrite)int  indexPage;

-(void)resetOrientation_ForWhosWatching;
- (IBAction)didClickOnBackFromWatching:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *watchingBackImage;

@property (weak, nonatomic) IBOutlet UITableView *watchingTable;

-(void)showLoadingViewForWatching;

@property (strong, nonatomic) IBOutlet CustomLoadingView *loadingForWatching;

@end
