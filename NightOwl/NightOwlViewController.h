//
//  NightOwlViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 3/20/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>


@class NightOwlAppDelegate;

@interface NightOwlViewController : UIViewController<UIScrollViewDelegate>
{
    int    currentIndex;
    
    NightOwlAppDelegate   *appDelegate;
    
    BOOL  justLaunched;
    UIImageView   *splashImageView;
    UIView        *splashImageBackgroundView;
    
    int   checkScroll;
}

@property (weak, nonatomic) IBOutlet UIScrollView *catagoryScrollerView;

-(void)LoadScrollviewWithCategory:(int)pageNumber CategoryTag:(int)_categoryTag;
-(void)displayNewCategory:(int)index CategoryTag:(int)_intcategoryTag;

-(void)resetOrientation_MediaViewer;
-(void)RemovePreviousViewsAndLoadViewsWithIndexNo:(int)_indexNo;

-(void)moveToNextCategory:(int)index;
-(void)moveToPreviousCategory:(int)index;

-(void)didClickOnSelectedCategory:(int)index;


-(void)showSplashImageView;
- (void)removeSplashImageView;

-(void)loadHomePage;

-(void)slideViewsWithAnimation:(int)_page;
-(void)LoadScrollviewWithCategoryWithAnimation:(int)pageNumber CategoryTag:(int)_categoryTag;
-(void)displayNewCategoryWithAnimation:(int)index CategoryTag:(int)_intcategoryTag;

@end
