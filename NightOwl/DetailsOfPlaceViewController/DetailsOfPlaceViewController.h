//
//  DetailsOfPlaceViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/12/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
//#import "CustomDelegate.h"
#import <Twitter/Twitter.h>

@class NightOwlAppDelegate;
@interface DetailsOfPlaceViewController : UIViewController<MKMapViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>
{
    NSString   *placeImageIcon;
    
    NightOwlAppDelegate   *appDelegate;
    
}

@property (weak, nonatomic) IBOutlet UIImageView *detailsBackImage;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceAddress;

@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;

@property (weak, nonatomic) IBOutlet RateView *detailsRateView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderActivity;
@property(nonatomic,readwrite)int      indexOfPage;

@property(nonatomic,strong)NSMutableDictionary  *detailsDic;

- (IBAction)didClickForBack:(id)sender;

-(void)loadDetailsOfPlace;

-(void)loadImageIcon:(NSString*)imageURL;
-(void)loadImageForDetails:(NSData*)imageData;
-(void)resetOrientation_ForDetails;

@property (weak, nonatomic) IBOutlet UIScrollView *holderScollerView;

@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNo;

- (IBAction)didClickGetDirection:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *getDirectionBtn;

@property (weak, nonatomic) IBOutlet UIView *lowerBtnHolderView;

@property (weak, nonatomic) IBOutlet UIImageView *holderMapView;

@property (weak, nonatomic) IBOutlet MKMapView *locationMapView;

-(void)showLocation;

@property (weak, nonatomic) IBOutlet UIView *backViewWithIMG;

- (IBAction)didClickOnFacebook:(id)sender;

- (IBAction)didClickOnTwitter:(id)sender;

- (IBAction)didClickOnMail:(id)sender;

- (IBAction)didClickOnSMS:(id)sender;

-(void) SendMailWithMailSubject: (NSString *)_mailSubject  mailBody : (NSString *) _mailBody;
-(void)showAlert:(NSString*)Msg   _sender:(id)sender  AlertTag:(int)alertTag;

-(BOOL)checkiOSVersion;

@end
