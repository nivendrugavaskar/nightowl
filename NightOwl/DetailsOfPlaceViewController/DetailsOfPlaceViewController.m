//
//  DetailsOfPlaceViewController.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/12/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "DetailsOfPlaceViewController.h"
#import "NightOwlAppDelegate.h"
#import "AnnotationClass.h"
#import "CustomAlertView.h"

@interface DetailsOfPlaceViewController ()

@end

@implementation DetailsOfPlaceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark-
#pragma mark  View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    appDelegate=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    [_holderMapView  setClipsToBounds:YES];
    
    _lblPlaceName.font=[UIFont  fontWithName:@"Cookies" size:kFONT_SIZE];
    _lblPlaceAddress.font=[UIFont  fontWithName:@"Cookies" size:kFONT_SIZE];
    
    _detailsRateView.notSelectedImage = [UIImage imageNamed:@"no_star.png"];
    _detailsRateView.halfSelectedImage = [UIImage imageNamed:@"half_star.png"];
    _detailsRateView.fullSelectedImage = [UIImage imageNamed:@"full_star.png"];
    _detailsRateView.rating = 0;
    _detailsRateView.editable = NO;
    _detailsRateView.maxRating = 5;
    
   // _imageIcon.layer.cornerRadius=4.0;
   // _imageIcon.layer.borderColor=[UIColor  whiteColor].CGColor;
   // _imageIcon.layer.borderWidth=2.0;
    
    [_imageIcon  setClipsToBounds:YES];
    
    _lblPlaceName.text=@"";
    _lblPlaceAddress.text=@"";
    _lblPhoneNo.text=@"";
    [_imageIcon  setImage:nil];
    
     //_holderScollerView.contentOffset=CGPointMake(0, 40);
    
   // NSLog(@"Details of dictionary :%@",_detailsDic);
    
    [self  performSelector:@selector(showLocation) withObject:nil afterDelay:0.0];
    
    
    [self resetOrientation_ForDetails];
    
}


#pragma mark-
#pragma mark  Show Location

-(void)showLocation
{
    CLLocationCoordinate2D location;
    
    if([[_detailsDic  valueForKey:@"p_lat"]  floatValue]>0 && [[_detailsDic  valueForKey:@"p_long"]  floatValue]>0)
    {
    
        location.latitude = [[_detailsDic  valueForKey:@"p_lat"]  floatValue];
        location.longitude = [[_detailsDic  valueForKey:@"p_long"]  floatValue];;
    
        AnnotationClass *newAnnotation = [[AnnotationClass alloc] initWithTitle:[_detailsDic  valueForKey:@"p_name"] AndCoordinate:location];
        [_locationMapView addAnnotation:newAnnotation];
    
        newAnnotation=nil;
    }
}


#pragma mark-
#pragma mark  Map View Delegate

//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
//{
//    //MKAnnotationView *annotationView = [views objectAtIndex:0];
//    
//    id <MKAnnotation> mp = [view annotation];
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1000, 1000);
//    [mapView setRegion:region animated:YES];
//    [mapView selectAnnotation:mp animated:YES];
//
//}

-(void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
    id <MKAnnotation> mp = [[views  objectAtIndex:0] annotation];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 1000, 1000);
    [mv setRegion:region animated:YES];
    [mv selectAnnotation:mp animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    
    static NSString* AnnotationIdentifier = @"AnnotationIdentifier";
    MKPinAnnotationView* pinView = [[MKPinAnnotationView alloc]
                                    initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];

    pinView.image = [UIImage imageNamed:@"pin_hotel.png"];
    
    pinView.canShowCallout=YES;
    
    
    return pinView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDetailsBackImage:nil];
    [self setLblPlaceName:nil];
    [self setLblPlaceAddress:nil];
    [self setImageIcon:nil];
    [self setDetailsRateView:nil];
    [self setLoaderActivity:nil];
    [self setHolderScollerView:nil];
    [self setLblPhoneNo:nil];
    [self setGetDirectionBtn:nil];
    
    [self setLowerBtnHolderView:nil];
    [self setHolderMapView:nil];
    [self setLocationMapView:nil];
    [self setBackViewWithIMG:nil];
    [super viewDidUnload];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super  viewWillAppear:animated];
    
    _holderScollerView.contentSize=CGSizeMake(_holderScollerView.frame.size.width,(_lowerBtnHolderView.frame.origin.y+_lowerBtnHolderView.frame.size.height+20));
    
    [self  loadDetailsOfPlace];
}

#pragma mark-
#pragma mark  Load Details Page

-(void)loadDetailsOfPlace
{
    _lblPlaceName.text=[_detailsDic  valueForKey:@"p_name"];
    _lblPlaceAddress.text=[_detailsDic  valueForKey:@"p_addrs"];
    
    if([self  checkRateValue:[_detailsDic  valueForKey:@"p_contact_no"]])
        [_lblPhoneNo  setText:[_detailsDic  valueForKey:@"p_contact_no"]];
    
    if([self  checkRateValue:[_detailsDic  valueForKey:@"rat_value"]])
        _detailsRateView.rating=[[_detailsDic  valueForKey:@"rat_value"] floatValue];
    
    if([[_detailsDic  valueForKey:@"p_image"] count]>0)
    {
    [self  loadImageIcon:[_detailsDic  valueForKey:@"p_image"]];
    }
}

#pragma mark-
#pragma mark  Check Null

-(BOOL)checkRateValue:(NSString*)rateVal
{
    if([rateVal   isKindOfClass:[NSNull  class]]){
        return NO;
    }else{
        return YES;
    }
}

#pragma mark-
#pragma mark  Load Image Icon

-(void)loadImageIcon:(NSArray*)imageURL
{
    
    NSString *imageurl=[imageURL lastObject];
   
   // placeImageIcon=[imageURL  lastPathComponent];
     placeImageIcon=[imageurl lastPathComponent];
    
    
    if([[NSFileManager defaultManager]  fileExistsAtPath:DOC_Path(placeImageIcon)])
    {
        
        if([UIImage  imageWithContentsOfFile:DOC_Path(placeImageIcon)])
            [_imageIcon  setImage:[UIImage  imageWithContentsOfFile:DOC_Path(placeImageIcon)]];
        
    }
    else{
        
        [_loaderActivity  setHidden:NO];
        [_loaderActivity  startAnimating];
        
        
        // new code
        imageurl = [imageurl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        imageurl = [imageurl stringByReplacingOccurrencesOfString:@"\n" withString:@"%20"];
        
        NSURL    *imageURl=[NSURL  URLWithString:imageurl];
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:imageURl];
        [request setHTTPMethod:@"POST"];
        
        NSOperationQueue *queue = [NSOperationQueue mainQueue];
        [NSURLConnection  sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
        
        {
            NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            if ([data length] > 0 && error == nil)
            {
                
                [self  performSelectorOnMainThread:@selector(loadImageForDetails:) withObject:data waitUntilDone:NO];
            }
            else if ([data length] == 0 && error == nil)
            {
                [_imageIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                
                [_loaderActivity  stopAnimating];
                [_loaderActivity  setHidden:YES];
            }
            else if (error != nil && error.code == NSURLErrorTimedOut) //used this NSURLErrorTimedOut from foundation error responses
            {
               // NSLog(@"Connection timed out: %@", error);
                
                [_imageIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                
                [_loaderActivity  stopAnimating];
                [_loaderActivity  setHidden:YES];
                
            }
            else if (error != nil)
            {
               // NSLog(@"Problem fetching data: %@", error);
                
                [_imageIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
                
                [_loaderActivity  stopAnimating];
                [_loaderActivity  setHidden:YES];
                
            }
            
        }];
        
    }

}

#pragma mark-
#pragma mark   Load Image

-(void)loadImageForDetails:(NSData*)imageData
{
    
    UIImage   *iconImg=[UIImage  imageWithData:imageData];
    
    if(iconImg){
        
        _imageIcon.alpha=0.0;
        
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3f];
        
        _imageIcon.alpha=1.0;
        [_imageIcon  setImage:iconImg];
        
        [UIView  commitAnimations];
        
        NSData *ConvertImageData =UIImageJPEGRepresentation(iconImg, 1.0);
        
        [[NSFileManager  defaultManager]  removeItemAtPath:DOC_Path(placeImageIcon) error:nil];
        
        [[NSFileManager defaultManager] createFileAtPath:DOC_Path(placeImageIcon) contents:ConvertImageData attributes:nil];
        
        [_loaderActivity  stopAnimating];
        [_loaderActivity  setHidden:YES];
        
    }else{
        
        [_imageIcon  setImage:[UIImage  imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Google_Places" ofType:@"png"]]];
        
        [_loaderActivity  stopAnimating];
        [_loaderActivity  setHidden:YES];
    }
    
}

#pragma mark-
#pragma mark  Click on back

- (IBAction)didClickForBack:(id)sender {
    
    [self.navigationController  popViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark  Orientation;

#ifdef  __IPHONE_6_0

-(BOOL)shouldAutorotate
{
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForDetails];
    return   UIInterfaceOrientationMaskAll;
    
}

#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForDetails];

    
    return YES;
    
    
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForDetails];
}


-(void)resetOrientation_ForDetails
{
   // _holderScollerView.contentOffset=CGPointMake(0, 40);
    
     [_holderMapView  setClipsToBounds:YES];
     _holderScollerView.contentSize=CGSizeMake(_holderScollerView.frame.size.width,(_lowerBtnHolderView.frame.origin.y+_lowerBtnHolderView.frame.size.height+20));
    
   // NSLog(@"%f",_holderScollerView.contentSize.height);
    
    if(appDelegate.orientation==UIInterfaceOrientationLandscapeLeft || appDelegate.orientation==UIInterfaceOrientationLandscapeRight){
        
        [_detailsBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailLandScapeArr  objectAtIndex:_indexOfPage]]];
        
        appDelegate.isLandScape=1;
        
       
    }
    else if(appDelegate.orientation==UIInterfaceOrientationPortraitUpsideDown){
        
        [_detailsBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexOfPage]]];
        
       
        
    }
    else{
        
        [_detailsBackImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexOfPage]]];
        
      
        
    }
    
}


#pragma mark-
#pragma mark  Get Direction

- (IBAction)didClickGetDirection:(id)sender {
    
    NSString *url =[NSString  stringWithFormat:@"http://maps.google.com/maps?daddr=%f,%f&saddr=%f,%f",[[_detailsDic  valueForKey:@"p_lat"]  floatValue],[[_detailsDic  valueForKey:@"p_long"]  floatValue],appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude];
    NSURL *urlForPathDirection =[NSURL  URLWithString:url];
    [[UIApplication sharedApplication] openURL:urlForPathDirection];
}

#pragma mark-
#pragma mark   Check Sysytem Version

-(BOOL)checkiOSVersion
{
    //True for iOS 6 or greater
    
    float   currentVersion=[[UIDevice currentDevice].systemVersion floatValue];
    
    if(currentVersion<6.000000)
        return YES;
    else
        return NO;
}



#pragma mark-
#pragma mark  Share Methods

#pragma mark  twitter


- (IBAction)didClickOnTwitter:(id)sender{
    
    if([self  checkiOSVersion])
    {
        
//        // new code
//        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
//        {
//        }
    
        
        TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
        
        [tweetViewController setInitialText:[NSString  stringWithFormat:@"Check out the place \nSpot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]];
        
        
        tweetViewController.completionHandler = ^(TWTweetComposeViewControllerResult result)
        {
            NSString *title = @"Twitter Status";
            NSString *msg=nil;
            
            if (result == TWTweetComposeViewControllerResultCancelled)
                msg = @"Twit is cancelled.";
            else if (result == TWTweetComposeViewControllerResultDone)
                msg = @"Twit Successful.";
            
            // Show alert to see how things went...
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            
        };
        
        [self  presentViewController:tweetViewController  animated:YES completion:^{
            ;
        }];
        tweetViewController=nil;
    
    }
    else
    {
    
        SLComposeViewController    *twitterSLComposerSheet=nil;
        
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            twitterSLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            
            [twitterSLComposerSheet  setInitialText:[NSString  stringWithFormat:@"Check out the place \nSpot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]];
            //[twitterSLComposerSheet addImage:[UIImage imageWithContentsOfFile:DOC_Path(placeImageIcon)]];
            
            
            [self  presentViewController:twitterSLComposerSheet animated:YES completion:^{
                ;
            }];
        }
        else {
            
            CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:@"Error" message:@"You need to setup an account in the Settings app under Twitter to use this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
            [customAlertView show];
            customAlertView=nil;
        }
        [twitterSLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            if (result == SLComposeViewControllerResultCancelled){
                
                [self  dismissViewControllerAnimated:YES completion:^{
                    ;
                }];
            }
            else if (result == SLComposeViewControllerResultDone){
                
                
                [self  dismissViewControllerAnimated:YES completion:^{
                    ;
                }];
            }
            
        }];

    }
}

#pragma mark  facebook

- (IBAction)didClickOnFacebook:(id)sender{
    
   /* SLComposeViewController    *facebookSLComposerSheet=nil;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        facebookSLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        [facebookSLComposerSheet  setInitialText:[NSString  stringWithFormat:@"Spot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]];
         [facebookSLComposerSheet addImage:[UIImage imageWithContentsOfFile:DOC_Path(placeImageIcon)]];
       
        
        [self  presentViewController:facebookSLComposerSheet animated:YES completion:^{
            ;
        }];
    }
    else {
                
        CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:@"Error" message:@"You need to setup an account in the Settings app under Twitter to use this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
        [customAlertView show];
        customAlertView=nil;
    }
    [facebookSLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        
        if (result == SLComposeViewControllerResultCancelled){
            
            [self  dismissViewControllerAnimated:YES completion:^{
                ;
            }];
        }
        else if (result == SLComposeViewControllerResultDone){
           
            
            [self  dismissViewControllerAnimated:YES completion:^{
                ;
            }];
        }
        
    }];*/
    
    appDelegate.posttoFB.isFacebookLogedin=YES;
    [appDelegate.posttoFB  postToWall:[NSString  stringWithFormat:@"Check out the place \nSpot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]];

}

#pragma mark mail

- (IBAction)didClickOnMail:(id)sender {
    
    [self  SendMailWithMailSubject:kAPP_ALERT_TITLE mailBody:[NSString  stringWithFormat:@"Check out the place \nSpot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]];
}

#pragma mark-
#pragma mark Alert Views  Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"%d",alertView.tag);
    
    switch (alertView.tag)
    {
        case ALERT_TAG_SUCCESS_SEND:
            [self dismissModalViewControllerAnimated:YES];
            break;
            
        case ALERT_TAG_MAIL_NOT_SENT:
            [self dismissModalViewControllerAnimated:YES];
            break;
                   
        case ALERT_TAG_MAIL_FAIL:
            [self dismissModalViewControllerAnimated:YES];
            break;
        case ALERT_TAG_MAIL_SAVED:
            [self dismissModalViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}


#pragma mark-
#pragma mark- Mail

-(void) SendMailWithMailSubject: (NSString *)_mailSubject  mailBody : (NSString *) _mailBody
{
    Class messageClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if (messageClass != nil)
    {
        // Check whether the current device is configured for sending SMS messages
        if ([messageClass canSendMail])
        {
            MFMailComposeViewController * mailController = [[MFMailComposeViewController alloc] init] ;
            mailController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            if ([MFMailComposeViewController canSendMail])
            {
                [mailController setSubject:_mailSubject];
                [mailController setMessageBody:_mailBody isHTML:YES];
                
                mailController.mailComposeDelegate = self;
                
                
                NSData *myData = [NSData dataWithContentsOfFile:DOC_Path(placeImageIcon)];
                
               
                    
                [mailController addAttachmentData:myData mimeType:@"image/jpg" fileName:kHOT_SOPT_IMAGE];
                    
                [self presentViewController:mailController animated:YES completion:^{
                    
                }];
                
                                
            }
            
            //mailController=nil;
            
        }
        else
            [self showAlert:ALT_MSG_NOT_CONFIGURED_DEVICE _sender:nil AlertTag:2];
    }
    
    
}

#pragma mark-
#pragma mark Mail Delgate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    if(result==MFMailComposeResultCancelled)
    {
        [self  showAlert:@"Mail cancelled!" _sender:self AlertTag:ALERT_TAG_MAIL_NOT_SENT];
    }
    else if(result==MFMailComposeResultFailed)
    {
        [self  showAlert:@"Mail sent fail!" _sender:self AlertTag:ALERT_TAG_MAIL_FAIL];
    }
    else if(result==MFMailComposeResultSent)
    {
        [self  showAlert:@"Mail sent sucessfully!" _sender:self AlertTag:ALERT_TAG_SUCCESS_SEND];
    }
    else if(result==MFMailComposeResultSaved)
    {
        [self  showAlert:@"Mail saved as draft!" _sender:self AlertTag:ALERT_TAG_MAIL_SAVED];
    }
    
    
}


#pragma mark-
#pragma mark   Show Alert

-(void)showAlert:(NSString*)Msg   _sender:(id)sender  AlertTag:(int)alertTag
{
    CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                     message:Msg
                                                                    delegate:sender
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil,nil];
    customAlertView.tag=alertTag;
    
    [customAlertView show];
    customAlertView=nil;
}

#pragma mark sms

- (IBAction)didClickOnSMS:(id)sender {
    
    if ([MFMessageComposeViewController canSendText])
    {
        MFMessageComposeViewController * messageController = [[MFMessageComposeViewController alloc] init] ;
        messageController.modalPresentationStyle = UIModalPresentationFullScreen;
        
        
        [messageController setBody:[NSString stringWithFormat:@"%@ \n %@",MAILSUBJECT,[NSString  stringWithFormat:@"Check out the place \nSpot Name:%@ \n Address:%@\nContact No:%@",_lblPlaceName.text,_lblPlaceAddress.text,_lblPhoneNo.text]]];
        
        messageController.messageComposeDelegate = self;
        
        
        
        [self presentViewController:messageController animated:YES completion:^{
            ;
        }];
        //messageController=nil;
        
    }
    else
        [self showAlert:ALT_MMS_NOT_CONFIGURED_DEVICE _sender:nil AlertTag:5];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self  dismissModalViewControllerAnimated:YES];
    
    if(result==MessageComposeResultCancelled)
    {
        [self  showAlert:@"Message  cancelled" _sender:nil AlertTag:6];
    }else if(result==MessageComposeResultSent)
    {
        [self  showAlert:@"Message  sent" _sender:nil AlertTag:7];
    }else if(result==MessageComposeResultFailed)
    {
        [self  showAlert:@"Message  send fails" _sender:nil AlertTag:8];
    }
}

@end
