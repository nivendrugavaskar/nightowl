//
//  ListOfEachCatagoryViewController.h
//  NightOwl
//
//  Created by Subhra_Limtex on 4/8/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListOfEachCatagoryViewController.h"
#import "CustomLoadingView.h"
#import "CustomDelegate.h"
#import "JSON.h"

@class NightOwlAppDelegate;
@interface ListOfEachCatagoryViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CustomDelegate>{

     NightOwlAppDelegate    *appDelegate;
    
    NSMutableArray     *listArr;
    
    int  indexForDetails;
}

@property (weak, nonatomic) IBOutlet UITableView *listOfCatagoryTable;

@property (strong, nonatomic) IBOutlet CustomLoadingView *customLoading;

- (IBAction)didClickOnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *backImage;

-(void)resetOrientation_ForList;

@property(nonatomic,readwrite)int   indexPage;
@property(nonatomic,strong)NSString   *categoryName;

-(void)loadCategoryList:(NSString*)searchName;
- (void)fetchedData:(NSData *)responseData;
-(void)startLocationUpdateInBackGround;

-(void)showLoadingView;
-(void)hidesLoadingView;

-(void)displayAlert:(NSString*)msg   _sender:(id)sender  alertTag:(int)_tag;

@property(nonatomic,readwrite)int  subcategory_Id;

-(NSString*)convertMeterToMiles:(NSString*)meterValue;

@end
