//
//  ListOfEachCatagoryViewController.m
//  NightOwl
//
//  Created by Subhra_Limtex on 4/8/13.
//  Copyright (c) 2013 Subhra_Limtex. All rights reserved.
//

#import "ListOfEachCatagoryViewController.h"
#import "NightOwlAppDelegate.h"
#import "CellDesigner.h"
#import "CustomCell.h"
#import "InitCustomLocation.h"
#import "DetailsOfPlaceViewController.h"
#import "CustomAlertView.h"

@interface ListOfEachCatagoryViewController ()

@end

@implementation UINavigationController (rotation)
//temp hack for iOS6, this allows passing supportedInterfaceOrientations to child viewcontrollers.
- (NSUInteger)supportedInterfaceOrientations {
    return [self.topViewController supportedInterfaceOrientations];
}

@end

@implementation ListOfEachCatagoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark-
#pragma mark  View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
     appDelegate=(NightOwlAppDelegate*)[[UIApplication  sharedApplication]  delegate];
    
    [self.view  addSubview:_customLoading];
    [_customLoading  initLoadingView];
    _customLoading.alpha=0.0;
    
    // new code
    _customLoading.holderView.center=CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    
    [self  resetOrientation_ForList];
    

}

-(void)viewWillAppear:(BOOL)animated
{
    [super  viewWillAppear:animated];
    
    [self  showLoadingView];
    
    [NSObject  cancelPreviousPerformRequestsWithTarget:self selector:@selector(startLocationUpdateInBackGround) object:nil];
    
    [self  performSelector:@selector(startLocationUpdateInBackGround) withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setListOfCatagoryTable:nil];
    [self setCustomLoading:nil];
    [self setBackImage:nil];
    [super viewDidUnload];
}

#pragma mark-
#pragma mark  Show Loading

-(void)showLoadingView
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _customLoading.alpha=1.0;
    [_customLoading  startLoading];
    
    [UIView   commitAnimations];

}

#pragma mark-
#pragma mark  Hide Loading

-(void)hidesLoadingView
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.5f];
    
    _customLoading.alpha=0.0;
    [_customLoading  stopLoading];
    
    [UIView   commitAnimations];
    
}


#pragma mark-
#pragma mark  Custom Delegate method

-(void)loadNearestPlaces
{
    [[InitCustomLocation  shareInstance]  setDelegate:nil];
    
    [NSObject  cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadCategoryList:) object:nil];
    [self  performSelector:@selector(loadCategoryList:) withObject:_categoryName afterDelay:0.5];
}


#pragma mark-
#pragma mark  Start Update Location 

-(void)startLocationUpdateInBackGround
{
    [[InitCustomLocation  shareInstance] initCustomLocation];
    [[InitCustomLocation  shareInstance]  setDelegate:self];
}


#pragma mark-
#pragma mark  Did Click on back

- (IBAction)didClickOnBack:(id)sender {
    
    if(_customLoading)
        [_customLoading  removeFromSuperview];
    
    [self.navigationController  popViewControllerAnimated:YES];
}


#pragma mark-
#pragma mark  Orientation;

#ifdef  __IPHONE_6_0

-(BOOL)shouldAutorotate
{
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForList];
    return   UIInterfaceOrientationMaskAll;
    
}
#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForList];
    
    return YES;
    
    
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    
    UIDeviceOrientation deviceOrientation = [[UIDevice  currentDevice] orientation];
    if(deviceOrientation == UIInterfaceOrientationPortrait || deviceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    else{
        appDelegate.orientation=(UIInterfaceOrientation)deviceOrientation;
    }
    
    [self  resetOrientation_ForList];
}


-(void)resetOrientation_ForList
{
    if(appDelegate.orientation==UIInterfaceOrientationLandscapeLeft || appDelegate.orientation==UIInterfaceOrientationLandscapeRight)
    {
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailLandScapeArr  objectAtIndex:_indexPage]]];
        
        appDelegate.isLandScape=1;
    }
    else if(appDelegate.orientation==UIInterfaceOrientationPortraitUpsideDown)
    {
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexPage]]];
        
    }
    else{
        
        [_backImage  setImage:[UIImage  imageNamed:[appDelegate.backImageDetailPotraitArr  objectAtIndex:_indexPage]]];
        
    }
    
}

#pragma mark-
#pragma mark  Convert Meter To Miles

-(NSString *)convertMeterToMiles:(NSString *)meterValue
{
    double km = [meterValue  floatValue]/1000;
    double miles = km / 1.6;
    NSString *mileString = [NSString  stringWithFormat:@"%.2f",miles];
    
    return mileString;
}


#pragma mark-
#pragma mark  Load SubCategory

-(void)loadCategoryList:(NSString*)searchName
{
    
    //https://maps.googleapis.com/maps/api/place/search/json?location=22.552698,88.424916&radius=5000&types=atm&sensor=false&key=AIzaSyDJbFnbZjeozOsZIKCoTcNCXtoN61Nq7J8
    
   // NSString   *urlString=[NSString  stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=%@&types=%@&sensor=false&key=%@",appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude,kFILTER_DISTANCE,searchName,kGOOGLE_API_KEY];
    
    NSLog(@"%@",[self  convertMeterToMiles:kFILTER_DISTANCE]);
    
    //http://www.businessprodemo.com/night_owl/index.php?r=product/productlist&id=5&lat=6.4462384636629695&long=-74.39883210937501&radius=5
    
//    NSString   *urlString=[NSString  stringWithFormat:@"%@r=product/productlist&id=%d&lat=%f&long=%f&radius=%.2f",kBASE_URL,_subcategory_Id,appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude,[[self  convertMeterToMiles:kFILTER_DISTANCE] floatValue]];
    
     NSString   *urlString=[NSString  stringWithFormat:@"%@r=api/productlist",kBASE_URL];
    
    //NSString   *urlString=[NSString  stringWithFormat:@"%@r=product/productlist&id=%d&lat=%f&long=%f&radius=%.2f",kBASE_URL,_subcategory_Id,22.145913084474763,88.34741550781246,[[self  convertMeterToMiles:kFILTER_DISTANCE] floatValue]];
     NSString *post = [NSString stringWithFormat:@"id=%d&lat=%f&long=%f&radius=%.2f",_subcategory_Id,appDelegate.currentLocation.coordinate.latitude,appDelegate.currentLocation.coordinate.longitude,1000.0f];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[post length]];
    
    
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"\n" withString:@"%20"];
    
    NSURL    *imageURl=[NSURL  URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:imageURl];
    [request setHTTPMethod:@"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSOperationQueue *queue = [NSOperationQueue mainQueue];
    [NSURLConnection  sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
        
         NSString *responseStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([data length] > 0 && error == nil)
        {
          [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:NO];
            
        }
        else if ([data length] == 0 && error == nil)
        {
            ;
        }
        else if (error != nil && error.code == NSURLErrorTimedOut) //used this NSURLErrorTimedOut from foundation error responses
        {
            //NSLog(@"Connection timed out: %@", error);
            
            [self  hidesLoadingView];
            [self  displayAlert:@"Connection Time Out!" _sender:self alertTag:2000];
            
        }
        else if (error != nil)
        {
            //NSLog(@"Problem fetching data: %@", error);
            
            [self  hidesLoadingView];
            [self  displayAlert:@"Error in Connection!" _sender:self alertTag:3000];
            
        }
        
    }];

}

#pragma mark-
#pragma mark  Fetch data

- (void)fetchedData:(NSData *)responseData
{
       
    NSString *retVal=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    
    retVal=[retVal stringByTrimmingCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSLog(@"Fetch data : %@",retVal);
       
     [self  hidesLoadingView];
    
    NSLog(@"%@",[[retVal  JSONValue]   valueForKey:@"data"]);
    
    listArr = [[retVal JSONValue] valueForKey:@"data"];
    
    NSLog(@"%@",listArr);
    
    if([listArr count]>0)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:1.0];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:_listOfCatagoryTable cache:YES];
        
        [_listOfCatagoryTable  reloadData];
        
        [UIView  commitAnimations];
    }
    else{
            
        [self  displayAlert:@"No Place found" _sender:self alertTag:1000];
            
    }
    
    retVal=nil;

}

#pragma mark-
#pragma mark  Show alert 

-(void)displayAlert:(NSString*)msg   _sender:(id)sender  alertTag:(int)_tag
{
  
    CustomAlertView *customAlertView = [[CustomAlertView alloc]initWithTitle:kAPP_ALERT_TITLE
                                                                     message:msg
                                                                    delegate:sender
                                                           cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil,nil];
    
    
     customAlertView.tag=_tag;
    [customAlertView show];
    customAlertView=nil;
}

#pragma mark-
#pragma mark  Alert view delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1000){
    
        if(buttonIndex==0){
            
            if(_customLoading)
                [_customLoading  removeFromSuperview];
            
            [self.navigationController  popViewControllerAnimated:YES];
        }
    }else if(alertView.tag==2000){
        
        if(buttonIndex==0){
            
            if(_customLoading)
                [_customLoading  removeFromSuperview];
            
            [self.navigationController  popViewControllerAnimated:YES];
        }
    }else if(alertView.tag==3000){
        
        if(buttonIndex==0){
            
            if(_customLoading)
                [_customLoading  removeFromSuperview];
            
            [self.navigationController  popViewControllerAnimated:YES];
        }
    }

}


#pragma mark-
#pragma mark  Table View DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listArr  count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    
    cell=(CustomCell*)[CellDesigner  getCustomCellForTable:tableView indexPath:indexPath nibName:@"CustomCell"];
    
    [(CustomCell*)cell  initialiseCell];
    
    if([listArr count]>0)
        [(CustomCell*)cell  loadListCell:[listArr  objectAtIndex:indexPath.row]];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView   *footerView=[[UIView  alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
    footerView.backgroundColor=[UIColor  clearColor];
    
    return footerView;
    
}


#pragma mark-
#pragma mark  Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    indexForDetails=indexPath.row;
    
    [self performSegueWithIdentifier:kPlaceDetailsVC sender: self];
}


#pragma mark-
#pragma mark  Prepare For Segue Action With Identifire

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:kPlaceDetailsVC]) {
        DetailsOfPlaceViewController *detailsVC = [segue destinationViewController];
        detailsVC.indexOfPage=_indexPage;
        detailsVC.detailsDic=[listArr  objectAtIndex:indexForDetails];
        
    }
}



@end
